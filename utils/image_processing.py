import time
import os
from copy import deepcopy

import cv2
import pytesseract
import numpy as np
from bs4 import BeautifulSoup
from imutils.object_detection import non_max_suppression

from utils.general_utils import get_secret

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


if get_secret("CUSTOM_TESSDATA_DIR"):
    TESSDATA_DIR = get_secret("TESSDATA_DIR")
else:
    TESSDATA_DIR = ""

TESSDATA_DIR_CONFIG = r'--tessdata-dir "{}" '.format(TESSDATA_DIR)
RESIZE_WIDTH = 1520
PATH_TO_EAST_TEXT_DETECTOR = get_secret("PATH_TO_EAST_TEXT_DETECTOR")
MINIMAL_PROBABILITY = 0.5


# def remove_lines_from_image(img):
#
#     img_height = img.shape[0]
#     img_width = img.shape[1]
#
#     gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
#     gray = cv2.GaussianBlur(gray, (5, 5), 0)
#
#     threshold = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 25, 10)
#
#     cv2.imwrite("/home/denny_k/PycharmProjects/korean_ocr/testdata/tmp_thresh0.png", threshold)
#
#     _, threshold = cv2.threshold(threshold, 0, 255, cv2.THRESH_BINARY_INV)
#
#     vertical_scale = int(img_height / 15.0)
#     horizontal_scale = int(img_width / 15.0)
#
#     kernel_v = cv2.getStructuringElement(cv2.MORPH_RECT, (1, vertical_scale))
#     vertical_mask = cv2.erode(threshold, kernel_v, iterations=1)
#     vertical_mask = cv2.dilate(vertical_mask, kernel_v, iterations=1)
#
#     kernel_h = cv2.getStructuringElement(cv2.MORPH_RECT, (horizontal_scale, 1))
#     horizontal_mask = cv2.erode(threshold, kernel_h, iterations=1)
#     horizontal_mask = cv2.dilate(horizontal_mask, kernel_h, iterations=1)
#
#     mask = vertical_mask + horizontal_mask
#
#     whiteImg = np.zeros(img.shape, img.dtype)
#     whiteImg[:, :] = (255, 255, 255)
#     whiteMask = cv2.bitwise_and(whiteImg, whiteImg, mask=mask)
#
#     kernel = np.ones((5, 5), np.uint8)
#     whiteMask = cv2.dilate(whiteMask, kernel, iterations=1)
#
#     cv2.addWeighted(whiteMask, 1, img, 1, 0, img)
#
#     return img


def ocr_from_box(img, box, img_id, language="eng",  is_alnum=False, is_alpha=False):
    data_boxes = list()
    x, y, w, h = box

    crop_img = img[y: y + h, x: x + w]



    crop_height = crop_img.shape[0]
    crop_width = crop_img.shape[1]

    new_img = np.zeros((crop_height + 100, crop_width + 100, 3), np.uint8)
    new_img[:, 0:new_img.shape[1]] = (255, 255, 255)
    new_img[50: 50 + crop_height, 30: 30 + crop_width] = crop_img

    # cv2.imwrite("/home/denny_k/PycharmProjects/korean_ocr/testdata/img_" + str(img_id) + "_orig.png", new_img)

    _, new_img = cv2.threshold(new_img, 55, 255, cv2.THRESH_BINARY)

    # cv2.imwrite("/home/denny_k/PycharmProjects/korean_ocr/testdata/img_" + str(img_id) + ".png", new_img)

    if TESSDATA_DIR:
        data = pytesseract.image_to_pdf_or_hocr(new_img, config=TESSDATA_DIR_CONFIG, extension='hocr', lang=language)
    else:
        data = pytesseract.image_to_pdf_or_hocr(new_img, extension='hocr', lang=language)

    # with open("/home/denny_k/PycharmProjects/korean_ocr/testdata/img_" + str(img_id) + ".hocr", "wb") as f:
    #     f.write(data)

    xml_data = BeautifulSoup(data, 'lxml-xml')
    spans = xml_data.find_all('span')

    # print(data)
    lines = list()
    for span in spans:
        if span['class'] == "ocr_line" and span.text.strip() and len(span.text.strip()) < 6:
            if is_alnum:
                data_boxes.append(
                    dict(data="".join(ch.strip() for ch in span.text.strip() if ch.isalnum()), box=(x, y, x + w, y + h)))
            elif is_alpha:
                data_boxes.append(
                    dict(data="".join(ch.strip() for ch in span.text.strip() if ch.isalpha()),
                         box=(x, y, x + w, y + h)))
            else:
                data_boxes.append(dict(data=span.text.strip(), box=(x, y, x + w, y + h)))
            lines.append(span)

    return data_boxes

# def filter_by_day_field(ocr_data):
#     korean_data = list()
#     for data_candidate in ocr_data['data_candidates']:
#         for day_candidate in ocr_data['day_candidates']:
#             if int(day_candidate[0]['box'][1]) < int(data_candidate[0]['box'][1]) + int(int(day_candidate[0]['box'][3]) / 2.0) < int(day_candidate[0]['box'][1]) + int(day_candidate[0]['box'][3]):
#                 korean_data.append(data_candidate)
#                 break
#     ocr_data['data_candidates'] = korean_data
#     return ocr_data


def east_detection(img_path):
    DEFAULT_RESIZE_WIDTH = 640
    DEFAULT_RESIZE_HEIGHT = 480

    start = time.time()

    img = cv2.imread(img_path)
    original_img = img.copy()
    draw_img = img.copy()
    orig_img_height, orig_img_width = img.shape[:2]
    img_area = orig_img_height * orig_img_width

    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    _, thresh1 = cv2.threshold(gray, 150, 255, cv2.THRESH_BINARY_INV)
    _, thresh2 = cv2.threshold(gray, 125, 255, cv2.THRESH_BINARY_INV)

    height_ratio = orig_img_height / float(DEFAULT_RESIZE_HEIGHT)
    width_ratio = orig_img_width / float(DEFAULT_RESIZE_WIDTH)

    img = cv2.resize(img, (DEFAULT_RESIZE_WIDTH, DEFAULT_RESIZE_HEIGHT))

    img_height, img_width = img.shape[:2]

    layerNames = [
        "feature_fusion/Conv_7/Sigmoid",
        "feature_fusion/concat_3"]

    net = cv2.dnn.readNet(PATH_TO_EAST_TEXT_DETECTOR)

    blob = cv2.dnn.blobFromImage(img, 1.0, (img_width, img_height),
                                 (123.68, 116.78, 103.94), swapRB=True, crop=False)

    net.setInput(blob)

    # show timing information on text prediction
    (scores, geometry) = net.forward(layerNames)

    # print(geometry)

    (numRows, numCols) = scores.shape[2:4]
    rects = []
    confidences = []


    for y in range(0, numRows):
        # extract the scores (probabilities), followed by the geometrical
        # data used to derive potential bounding box coordinates that
        # surround text
        scoresData = scores[0, 0, y]
        xData0 = geometry[0, 0, y]
        xData1 = geometry[0, 1, y]
        xData2 = geometry[0, 2, y]
        xData3 = geometry[0, 3, y]
        anglesData = geometry[0, 4, y]

        for x in range(0, numCols):
            # if our score does not have sufficient probability, ignore it
            if scoresData[x] < MINIMAL_PROBABILITY:
                continue

            # compute the offset factor as our resulting feature maps will
            # be 4x smaller than the input image
            (offsetX, offsetY) = (x * 4.0, y * 4.0)

            # extract the rotation angle for the prediction and then
            # compute the sin and cosine
            angle = anglesData[x]
            cos = np.cos(angle)
            sin = np.sin(angle)

            # use the geometry volume to derive the width and height of
            # the bounding box
            h = xData0[x] + xData2[x]
            w = xData1[x] + xData3[x]

            # compute both the starting and ending (x, y)-coordinates for
            # the text prediction bounding box
            endX = int(offsetX + (cos * xData1[x]) + (sin * xData2[x]))
            endY = int(offsetY - (sin * xData1[x]) + (cos * xData2[x]))
            startX = int(endX - w)
            startY = int(endY - h)

            # add the bounding box coordinates and probability score to
            # our respective lists
            rects.append((startX, startY, endX, endY))
            confidences.append(scoresData[x])

    # print(confidences)

    boxes = non_max_suppression(np.array(rects), probs=confidences)

    boxes = sorted(boxes, key=lambda srt_box: srt_box[1])

    # loop over the bounding boxes
    end = time.time()

    # show timing information on text prediction
    print("[INFO] text detection took {:.6f} seconds".format(end - start))

    data_boxes = list()

    crop_counter = 1

    ocr_data = dict()

    ocr_data['day_candidates'] = list()
    ocr_data['time_candidates'] = list()
    ocr_data['data_candidates'] = list()
    img_id = 1
    for (startX, startY, endX, endY) in boxes:
        # scale the bounding box coordinates based on the respective
        # ratios
        # crop_path = working_dir + "cropped_img_" + str(crop_counter) + ".png"
        startX = int((startX - 2) * width_ratio) if int((startX - 2) * width_ratio) > 0 else 1
        startY = int((startY - 2) * height_ratio) if int((startY - 2) * height_ratio) > 0 else 1
        endX = int((endX + 2) * width_ratio) if int((endX + 2) * width_ratio) < orig_img_width else orig_img_width - 1
        endY = int((endY + 2) * height_ratio) if int((endY + 2) * height_ratio) < orig_img_height else orig_img_height - 1

        # crop_img = original_img[startY: endY, startX: endX]
        #
        # data = pytesseract.image_to_pdf_or_hocr(crop_img, config=TESSDATA_DIR_CONFIG, extension='hocr')
        #
        # xml_data = BeautifulSoup(data, 'lxml-xml')
        # spans = xml_data.find_all('span')
        #
        # # print(data)
        # lines = list()
        # for span in spans:
        #     if span['class'] == "ocr_line" and span.text.strip():
        #         data_boxes.append(dict(data=span.text.strip(), box=(startX, startY, endX, endY)))
        #         lines.append(span)
        # # cv2.imwrite(crop_path, crop_img)

        # draw the bounding box on the image

        rect_width = endX - startX
        rect_height = endY - startY

        if rect_width / rect_height > 1:

            # print("{} {} {} {}".format(startX, startY, w, rect_height))

            if startX < orig_img_height / 10:
                candidate = ocr_from_box(original_img, (startX, startY, rect_width, rect_height), img_id, "eng", is_alpha=True)
                print(candidate)
                if candidate:
                    # print(candidate)
                    ocr_data['day_candidates'].append(candidate)
            # elif startY < orig_img_width / 15:
            #     candidate = ocr_from_box(original_img, (startX, startY, rect_width, rect_height), img_id, "eng")
            #     if candidate:
            #         ocr_data['time_candidates'].append(candidate)
            else:
                candidate = ocr_from_box(original_img, (startX, startY, rect_width, rect_height), img_id, "eng")
                if candidate:
                    ocr_data['data_candidates'].append(candidate)
                else:
                    print("Candidate {} is empty".format(img_id))

            img_id += 1
            cv2.rectangle(draw_img, (startX, startY), (endX, endY), (0, 255, 0), 15)

        crop_counter += 1
    cv2.imwrite("/home/denny_k/PycharmProjects/korean_ocr/testdata/draw_img.png", draw_img)

    return ocr_data


# def text_box_detection(img_path):
#
#     img = cv2.imread(img_path)
#     orig = deepcopy(img)
#     orig_nolines = remove_lines_from_image(orig)
#     draw_img = deepcopy(img)
#
#     orig_img_height = img.shape[0]
#     orig_img_width = img.shape[1]
#     orig_img_area = orig_img_height * orig_img_width
#
#     resize_ratio = orig_img_width / float(RESIZE_WIDTH)
#     RESIZE_HEIGHT = int(orig_img_height / float(resize_ratio))
#
#     img = cv2.resize(img, (RESIZE_WIDTH, RESIZE_HEIGHT))
#
#     img = remove_lines_from_image(img)
#
#     img_height = img.shape[0]
#     img_width = img.shape[1]
#     img_area = img_height * img_width
#
#     cv2.imwrite("/home/denny_k/PycharmProjects/korean_ocr/testdata/tmp_resized.png", img)
#
#     gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
#     gray = cv2.GaussianBlur(gray, (5, 5), 0)
#     threshold = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 25, 10)
#
#     cv2.imwrite("/home/denny_k/PycharmProjects/korean_ocr/testdata/tmp_thresh0.png", threshold)
#
#     _, threshold = cv2.threshold(threshold, 0, 255, cv2.THRESH_BINARY_INV)
#
#     cv2.imwrite("/home/denny_k/PycharmProjects/korean_ocr/testdata/tmp_thresh1.png", threshold)
#
#     element = cv2.getStructuringElement(cv2.MORPH_RECT, (14, 14))
#
#     threshold = cv2.morphologyEx(threshold, cv2.MORPH_CLOSE, element)
#
#     cv2.imwrite("/home/denny_k/PycharmProjects/korean_ocr/testdata/tmp_thresh2.png", threshold)
#
#     kernel = np.ones((3, 3), np.uint8)
#     threshold = cv2.dilate(threshold, kernel, iterations=3)
#     # threshold = cv2.erode(threshold, kernel, iterations=2)
#
#     # threshold = cv2.morphologyEx(threshold, cv2.MORPH_OPEN, kernel)
#
#     # cv2.imshow("After morphing", threshold)
#     # cv2.waitKey(0)
#     # cv2.destroyAllWindows()
#
#     cv2.imwrite("/home/denny_k/PycharmProjects/korean_ocr/testdata/tmp_thresh3.png", threshold)
#
#     cnts, __ = cv2.findContours(threshold, 0, 1)
#     poly_list = list()
#     rects = list()
#
#     ocr_data = dict()
#
#     ocr_data['day_candidates'] = list()
#     ocr_data['time_candidates'] = list()
#     ocr_data['data_candidates'] = list()
#     img_id = 1
#     for cnt in cnts:
#         candidate = ""
#         x, y, w, h = cv2.boundingRect(cnt)
#
#         # x = int(x * resize_ratio)
#         # y = int(y * resize_ratio)
#         # w = int(w * resize_ratio)
#         # h = int(h * resize_ratio)
#
#         # new_img = cv2.adaptiveThreshold(new_img, 255, cv2.ADAPTIVE_THRESH_MEAN_C,
#         #                                 cv2.THRESH_BINARY, 75, 45)
#
#         if w * h > img_area / 1500:
#             # print("{} {} {} {}".format(x, y, w, h))
#
#             if x < img_height / 10:
#                 candidate = ocr_from_box(img, (x, y, w, h), img_id, "eng", is_alnum=True)
#                 if candidate:
#                     ocr_data['day_candidates'].append(candidate)
#             elif y < img_height / 10:
#                 candidate = ocr_from_box(img, (x, y, w, h), img_id, "eng")
#                 if candidate:
#                     ocr_data['time_candidates'].append(candidate)
#             else:
#                 candidate = ocr_from_box(img, (x, y, w, h), img_id, "kor", is_alpha=True)
#                 if candidate:
#                     ocr_data['data_candidates'].append(candidate)
#                 else:
#                     print("Candidate {} is empty".format(img_id))
#
#             cv2.rectangle(img, (x, y), (x + w, y + h), (0, 0, 255), 2)
#             img_id += 1
#
#     ocr_data = filter_by_day_field(ocr_data)
#     print(ocr_data)
#     print(len(ocr_data['data_candidates']))
#
#     cv2.imwrite("/home/denny_k/PycharmProjects/korean_ocr/testdata/tmp_contours.png", img)
