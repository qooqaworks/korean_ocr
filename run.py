import json

from utils.image_processing import east_detection

def main_chain(file_path):
    result = east_detection(file_path)
    return result