import os
import time

import aiohttp_cors
from aiohttp import web

from run import main_chain

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

UPLOADS_DIR = BASE_DIR + "/uploads/"


async def handle(request):
    if request.method == "POST":
        template = str()
        file_name = str()
        response = "Initial response. If you see this then something went wrong!"
        file_data = None
        reader = await request.multipart()
        # Read POST request
        while True:
            part = await reader.next()
            if part is None:
                break
            filedata = await part.read(decode=False)
            if part.name == 'file':
                file_name = part.filename
                file_data = filedata

        if file_data and file_name:
            file_name = file_name.replace(" ", "_")
            file_name = file_name.replace("(", "")
            file_name = file_name.replace(")", "")
            dir_path = UPLOADS_DIR + str(int(time.time())) + "_tt_" + file_name.rsplit(".", 1)[0] + "/"
            file_path = dir_path + file_name
            if not os.path.exists(dir_path):
                os.makedirs(dir_path)

            try:
                with open(file_path, 'wb') as f:
                    f.write(file_data)
            except TypeError:
                with open(file_path, 'w') as f:
                    f.write(file_data)

            response = main_chain(file_path)

        else:
            response = "ERROR: No pdf attached or wrong key given. Please provide pdf document in form-data with key 'input_pdf'"
    else:
        response = "ERROR: Wrong request type. Please make POST request instead of {}".format(request.method)

    return web.json_response(response)


if __name__ == "__main__":

    app = web.Application()
    cors = aiohttp_cors.setup(app, defaults={
        "*": aiohttp_cors.ResourceOptions(
            allow_credentials=True,
            expose_headers="*",
            allow_headers="*",
        )
    })

    cors.add(app.router.add_post('/korean_ocr', handle))

    web.run_app(app, port=8010)